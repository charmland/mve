/*
 * Copyright (C) 2015, Simon Fuhrmann
 * TU Darmstadt - Graphics, Capture and Massively Parallel Computing
 * All rights reserved.
 *
 * This software may be modified and distributed under the terms
 * of the BSD 3-Clause license. See the LICENSE.txt file for details.
 */

#ifndef MVE_SCENE_HEADER
#define MVE_SCENE_HEADER

#include <vector>
#include <string>
#include <memory>

#include "mve/defines.h"
#include "mve/view.h"
#include "mve/bundle.h"

#define MVE_SCENE_VIEWS_DIR "views/"
#define MVE_SCENE_BUNDLE_FILE "synth_0.out"

MVE_NAMESPACE_BEGIN

/**
 * Scene representation for the MVE.
 * A scene is loaded by specifying the scene root directory. The following
 * files and directories are recognized within the scene root:
 *
 * - directory "views": contains the views in the scene.
 * - file "synth_0.out": bundle file that contains key points.
 */
class Scene
{
public:
    typedef std::shared_ptr<Scene> Ptr;
    typedef std::vector<View::Ptr> ViewList;

public:
    /** Constructs and loads a scene from the given directory. */
    static Scene::Ptr create (std::string const& path);

    Scene(const Scene&) = delete;
    Scene& operator=(const Scene&) = delete;

    /** Loads the scene from the given directory. */
    void load_scene (std::string const& base_path);

    /** Returns the list of views. */
    ViewList const& get_views() const;
    /** Returns the list of views. */
    ViewList& get_views();
    /** Returns the bundle structure. */
    Bundle::ConstPtr get_bundle();
    /** Sets a new bundle structure. */
    void set_bundle (Bundle::Ptr bundle);
    /** Resets the bundle file such that it is re-read on get_bundle. */
    void reset_bundle();

    /** Returns a view by ID or 0 on failure. */
    View::Ptr get_view_by_id (std::size_t id);

    /** Saves bundle file if dirty as well as dirty embeddings. */
    void save_scene();
    /** Saves dirty embeddings only. */
    void save_views();
    /** Saves the bundle file if dirty. */
    void save_bundle();
    /** Forces rewriting of all views. Can take a long time. */
    void rewrite_all_views();

    /** Returns true if one of the views or the bundle file is dirty. */
    bool is_dirty() const;

    /** Returns the base path of the scene. */
    std::string const& get_path() const;

    /** Forces cleanup of unused embeddings. */
    void cache_cleanup();

    /** Returns total scene memory usage. */
    std::size_t get_total_mem_usage();
    /** Returns view memory usage. */
    std::size_t get_view_mem_usage();
    /** Returns key point memory usage. */
    std::size_t get_bundle_mem_usage();

protected:
    Scene();

private:
    std::string basedir;
    ViewList views;
    Bundle::Ptr bundle;
    bool bundle_dirty;

private:
    void init_views();
};

/* ---------------------------------------------------------------- */

inline
Scene::Scene()
    : bundle_dirty(false)
{
}

inline Scene::Ptr
Scene::create (std::string const& path)
{
    Scene::Ptr scene(new Scene);
    scene->load_scene(path);
    return scene;
}

inline Scene::ViewList const&
Scene::get_views() const
{
    return this->views;
}

inline Scene::ViewList&
Scene::get_views()
{
    return this->views;
}

inline View::Ptr
Scene::get_view_by_id (std::size_t id)
{
    return (id < this->views.size() ? this->views[id] : View::Ptr());
}

inline std::string const&
Scene::get_path() const
{
    return this->basedir;
}

inline void
Scene::set_bundle (Bundle::Ptr bundle)
{
    this->bundle_dirty = true;
    this->bundle = bundle;
}

inline void
Scene::reset_bundle()
{
    this->bundle.reset();
    this->bundle_dirty = false;
}

MVE_NAMESPACE_END

#endif /* MVE_SCENE_HEADER */
